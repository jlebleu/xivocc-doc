********
ACD Call
********

ACD outgoing calls
==================

By default, when your agents process incoming and outgoing calls, the call distribution will not take into account agents
which are in outgoing calls in the *least recent* call strategy and at the end of an outgoing call there is no wrapup.
So an agent can be distributed just after an outgoing calls even if another agent is free for a longer time, because the
outgoing call is not taken into account by the distribution strategy.

You will find below how to improve that.

XiVO-CC agent can make outgoing calls through an outgoing queue. This brings the statistics and
supervision visualization for outgoing ACD calls. However, some special configuration steps are
required:

* You need to create an outgoing queue with a name starting with 'out', e.g. outgoing_queue.
* This queue must be configured with preprocess subroutine xuc_outcall_acd, without On-Hold Music
  (tab General), Ringing Time must be 0 and Ring instead of On-Hold Music must be activated (both
  tab Application).  
* The subroutine must be deployed on the Xivo server (to /etc/asterisk/extension_extra.d/ or
  through the web interface), the file is available from 
  https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/xuc_outcall_acd.conf, with owner
  asterisk:www-data and rights 660.
* You must also deploy the file
  https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/generate_outcall_skills.py to
  /usr/local/sbin/, with owner root:root and rights 755.
* Furthermore, you must replace the file /etc/asterisk/queueskills.conf by the following one
  https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/queueskills.conf (be sure to backup
  the original one), without changing the owner or rights
* And finally you need to add a new skill rule on the Xivo server:
  :menuselection:`Services --> Call center --> Skill rules -> Add`, with name 'select_agent' and rules '$agent > 0'.

Once done, calls requested by an agent through the Cti.js with more than 6 digits are routed via
the outgoing queue. You can change the number of digits using the parameter xuc.outboundLength
in the XuC's configuration.
