.. _installation:

********
Overview
********

The content below describes how to install XiVO-CC on a single machine.

The following components will be installed:

* **PostgreSQL** : Database server for configuration and statistics
* **Configuration Management**: Configuration and permission provider used by XuC and Recording Server to manage the user rights
* **XuC**: outsourced CTI server providing telephony events, statistics and commands through a WebSocket
* **XuC Management**: supervision web pages, CCAgent and Web Assistant
* **SpagoBI**: BI suite with default statistic reports based on the Pack Reporting
* **Recording Server**: web server allowing to search recorded conversations
* **Pack Reporting**: statistic summaries stored in a PostgreSQL database
* **Totem Support**: near-real time statistics based on ElasticSearch_

.. _ElasticSearch: https://www.elastic.co/

.. figure:: images/xivoccarchitecture.png
    :scale: 80%
    :alt: XiVOcc reporting architecture

************
Requirements
************

Your server must meet the following requirements:

* `Debian 8`_ (jessie), 64 bit
* Docker_ installed
* `Docker Compose`_ installed
* the XiVO PBX is reachable on the network
* the XiVO PBX is setup with users, queues and agents, you must be able to place and answer calls.

.. _Debian 8: <https://www.debian.org/CD/netinst/>
.. _Docker: <https://docs.docker.com/engine/installation/linux/debian/#/debian-jessie-80-64-bit>
.. _Docker Compose: <https://docs.docker.com/compose/install/>

.. note:: Use only released version of *Docker* and *Docker Compose*

**********************
XiVO PBX Configuration
**********************

.. warning:: All configuration below have to be done on the XiVO PBX

XiVO PBX Restrictions
=====================

XiVO PBX enables a wide range of configuration, XiVO-CC is tested and validated with a number of
restriction concerning configurations of XiVO PBX:

* Queue ringing strategy should not be *Ring All*
* All users and queues have to be in the same context
* Do not activate Contexts Separation in *xivo-ctid* Configuration
* Agent and Supervisors profiles should use the same Presence Group
* Agents and Phones should be in the same context for mobile agents
* Do not use pause on one queue status, in queue advanced configuration, autopause should be No or
  All

PostgreSQL Configuration
========================

A number of XiVO-CC components need to access directly to the XiVO PBX database.

To allow this, we will set up the remote access, configure authentication, and add a new user for
the statistics to the PostgreSQL instance of your XiVO PBX.

Allow Remote access and authentication to PostgreSQL
----------------------------------------------------

Edit files as bellow:

* :file:`/etc/postgresql/9.4/main/postgresql.conf` around line 59 change the `listen_addresses` as bellow:

.. code-block:: ini

   listen_addresses = '*'

* :file:`/etc/postgresql/9.4/main/pg_hba.conf` add this line at the end of the file 

.. code-block:: ini

   host asterisk all <XiVO-CC-IP>/32 md5

.. note:: Replace <XiVO-CC-IP> by the **real** XiVO-CC IP address of your server.

Create a new user for statistics in the database
------------------------------------------------

Create a user `stats` with read permissions:

.. code-block:: bash

   sudo -u postgres psql asterisk << EOF
   CREATE USER stats WITH PASSWORD 'stats';
   GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO stats;
   EOF

Apply the configurations
------------------------


To apply the settings, we need to restart all XiVO Services with the following command:

.. code-block:: bash

   xivo-service restart all

AMI Configuration
=================

The XuC need to access to the :abbr:`AMI (Asterisk Manager Interface)` of your XiVO PBX.

In this section, we will see how to configure asterisk to allow this.

XiVO PBX < 15.18
----------------

Add the following lines in :file:`/etc/asterisk/manager.conf` :

.. code-block:: ini

   [xuc]
   secret = xucpass
   deny=0.0.0.0/0.0.0.0
   permit=<XiVO-CC-IP>/255.255.255.255
   read = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
   write = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan

.. note:: Replace <XiVO-CC-IP> by the **real** XiVO-CC IP address of your server.

XiVO PBX ≥ 15.18
----------------

Create a file named :file:`/etc/asterisk/manager.d/02-xivocc.conf` and add following lines in.

.. code-block:: ini

   [xuc]
   secret = xucpass
   deny=0.0.0.0/0.0.0.0
   permit=<XiVO-CC-IP>/255.255.255.255
   read = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
   write = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan

.. note:: Replace <XiVO-CC-IP> by the **real** XiVO-CC IP address of your server.

Apply the configurations
------------------------

To apply the settings, we need to reload the AMI:

.. code-block:: bash

   asterisk -rx "manager reload"


To verify that the configuration is correctly applied, you should use the following command and check
if your previous configuration is displayed.

.. code-block:: bash

   asterisk -rx "manager show user xuc"

CEL Configuration
=================

XiVO-CC needs more CEL events than those configured by default in XiVO PBX

XiVO PBX ≤ 15.12
----------------

Prior to version 15.12 XiVO PBX uses Asterisk in version 11.

Edit like below :file:`/etc/asterisk/cel.conf` :

.. code-block:: ini

   [general]
   enable=yes
   apps=dial,park,queue
   events=APP_START,CHAN_START,CHAN_END,ANSWER,HANGUP,BRIDGE_START,BRIDGE_END,BRIDGE_UPDATE,USER_DEFINED,LINKEDID_END,HOLD,UNHOLD,BLINDTRANSFER,ATTENDEDTRANSFER

   [manager]
   enabled=yes

XiVO PBX ≥ 15.13
----------------

Since the version 15.13 XiVO PBX uses Asterisk in version 13.

Edit like below :file:`/etc/asterisk/cel.conf` :

.. code-block:: ini

   [general]
   enable = yes
   apps = dial,park,queue
   events = APP_START,CHAN_START,CHAN_END,ANSWER,HANGUP,BRIDGE_ENTER,BRIDGE_EXIT,USER_DEFINED,LINKEDID_END,HOLD,UNHOLD,BLINDTRANSFER,ATTENDEDTRANSFER

   [manager]
   enabled = yes

Apply the configurations
------------------------

To apply the settings, we need to reload the CEL module:

.. code-block:: bash

   asterisk -rx "module reload cel"

Customizations in the XiVO PBX Web Interface
============================================

Enable Multiqueues call stats sharing
-------------------------------------

This option make the lastcall and calls received be the same for members logged in more than one
queue.

This is useful to make the queue respect the wrapuptime of another queue for an agent.

XiVO PBX ≤ 15.10
++++++++++++++++

You have to Enable *Multiqueues call stats sharing* in :menuselection:`General Settings --> Advanced --> Queues Tab`.

XiVO PBX > 15.10
++++++++++++++++

This option is checked by default.

Create a XuC CTI user
---------------------

The XuC have to connect to *xivo-ctid*, so we need to create a user XuC in :menuselection:`Services
--> IPBX --> Users`
with at least the following parameters:

+------------+------------+
|First name  | xuc        |
+------------+------------+
| Login      | xuc        |
+------------+------------+
| Password   | 0000       |
+------------+------------+
| Profile    | Supervisor |
+------------+------------+

.. warning:: This user is for XiVOxc internal use only, do not use it in any application.

Create a Web Services user
--------------------------

The XuC have to connect to XiVO PBX webservices, so we need to create a user in
:menuselection:`Configuration --> Web Services Access` with the following parameters :

+-------------+----------------+
| Name        | xivows         |
+-------------+----------------+
| Login       | xivows         |
+-------------+----------------+
| Password    |                |
+-------------+----------------+
| Host        | <XiVO-CC-IP>   |
+-------------+----------------+
| Description | XiVO-CC WS     |
+-------------+----------------+

.. note:: Replace <XiVO-CC-IP> by the **real** XiVO-CC IP address of your server.


Phone integration
=================

XuC based web applications like agent interface integrates buttons for phone
control. To remotely control the agents phones, your server need to :

* be able to reach your phones through the VOIP network
* :ref:`have compatible phone<phone_integration_supported>`
* :ref:`have your phones correctly configured<phone_integration_installation>`

Packages for the recording
==========================

Installation
------------

To use the recording feature, you must create and install on the XiVO PBX the debian package
available in the following git repository: ``https://gitlab.com/xivoxc/xivo-recording.git``

At the moment there is no public repository, but if you host the package in a private repository,
you can install the package like this:

.. code-block:: bash

   apt-get update
   apt-get install xivo-recording

During the installation, you will be asked for :

* the recording server IP : enter the XiVO-CC IP address 
* the XiVO PBX name (it **must** not contain any space or "-" character).

If you have several XiVO PBX, you **must** give a different name to each of them.

Activation
----------

This package has installed two dialplan subroutines :

* ``xivoxc-recording-incall``: used to record incoming calls
* ``xivoxc-recording-outcall``: used to record outgoing calls

To record calls, you must place the subroutines where you want to perform the recording action.

*********************
XiVO-CC Configuration
*********************

.. warning:: All configuration below have to be done on the XiVO-CC server.

Pre-installation
================

Install ntp server
------------------

On a minimal Debian installation, the ntp server is not installed, the XiVO-CC and the XiVO PBX 
time must be synchronized to the same source.

Bellow how to install the ntp service :

.. code-block:: bash

    apt-get install ntp

Create LogRotate for Docker
---------------------------

Docker container log output to :file:`/dev/stdout` and :file:`/dev/stderr`.
The Docker container log file is saved in 
:file:`/var/lib/docker/containers/[CONTAINER ID]/[CONTAINER_ID]-json.log`.

Log rotation provide dated and compressed logfile.

To do that, create a new logrotate configuration file for your Docker containers in 
:file:`/etc/logrotate.d/docker-container`.

.. code-block:: ini

        /var/lib/docker/containers/*/*.log {
          rotate 7
          daily
          compress
          missingok
          delaycompress
          copytruncate
        }


You can test it with ``logrotate -fv /etc/logrotate.d/docker-container``.

You should get some output and new log files will be created with the following syntax 
``[CONTAINER ID]-json.log.1``.

Those files will be compressed in next rotation cycle.

Installation
============

Retrieve the configuration script and launch it:

.. code-block:: bash

   wget  https://gitlab.com/xivoxc/packaging/raw/master/install/install-docker-xivocc.sh
   bash install-docker-xivocc.sh

During the installation, you will be asked for:

* the XiVO PBX IP address
* the number of weeks to keep the statistics
* the number of weeks to keep the recording files
* the external IP of the machine (the XiVO-CC IP address)

To simplify administration you can create the following alias in your .bashrc file:

.. code-block:: bash

    vi ~/.bashrc
    alias dcomp='docker-compose -p xivocc -f /etc/docker/compose/docker-xivocc.yml'

Adjust the default configuration
===================================

Depending on the version of your XiVO PBX it is necessary to make changes to the file :file:`/etc/docker/compose/docker-xivocc.yml`.

XiVO PBX ≤ 15.12
----------------

These XiVO PBX versions are using the version 11 of asterisk.

To make your installation compatible, you must change the tag of ``xuc`` and ``xivo-full-stats`` images
containers with ``latestast11``.

To do that, edit :file:`/etc/docker/compose/docker-xivocc.yml`:

* around line 40 change:

.. code-block:: yaml

    xivo_stats:
        image: xivoxc/xivo-full-stats:latestast13


to

.. code-block:: yaml

    xivo_stats:
        image: xivoxc/xivo-full-stats:latestast11

* around line 186 change:

.. code-block:: yaml

    xuc:
        image: xivoxc/xuc:latestast13

to

.. code-block:: yaml

    xuc:
        image: xivoxc/xuc:latestast11


XiVO PBX 15.12 < 16.01
----------------------

These XiVO PBX versions are using the version 13 of asterisk.

To make your installation compatible, you must change the tag of ``xivo-full-stats`` image
container.

To do that, edit :file:`/etc/docker/compose/docker-xivocc.yml`:

* around line 40 change:

.. code-block:: yaml

    xivo_stats:
        image: xivoxc/xivo-full-stats:latestast13

to

.. code-block:: yaml

    xivo_stats:
        image: xivoxc/xivo-full-stats:latestxivo16


XiVO PBX = 16.04
----------------

These XiVO PBX versions introduce some change in the XiVO CTI protocol.

To make your installation compatible, you must change the tag for ``xuc`` image container with ``latestxivo16``.

To do that, edit :file:`/etc/docker/compose/docker-xivocc.yml`:

* around line 186 change:

.. code-block:: yaml

    xuc:
        image: xivoxc/xuc:latestast13

to

.. code-block:: yaml

    xuc:
        image: xivoxc/xuc:latestxivo16


Download the modified images
----------------------------

You can download the modified images with this command:

.. code-block:: bash

    dcomp pull


Starting XiVO-CC
================

We configured the server and downloaded all the XiVO-CC components, we can now start them with:

.. code-block:: bash

   dcomp up -d

Post-installation and first checking
====================================

Docker container
----------------

Check that all container are running
++++++++++++++++++++++++++++++++++++

You can list all the XiVO-CC services, using:

.. code-block:: bash

    dcomp ps

And get the following informations:

.. code-block:: ini

            Name                         Command               State                                 Ports                              
  --------------------------------------------------------------------------------------------------------------------------------------
  xivocc_config_mgt_1         bin/config-mgt-docker            Up       0.0.0.0:9100->9000/tcp                                          
  xivocc_elasticsearch_1      /docker-entrypoint.sh elas ...   Up       0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp                  
  xivocc_fingerboard_1        /bin/sh -c /usr/bin/tail - ...   Up                                                                       
  xivocc_kibana_volumes_1     /bin/sh -c /usr/bin/tail - ...   Up                                                                       
  xivocc_nginx_1              nginx -g daemon off;             Up       0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp, 0.0.0.0:8443->8443/tcp
  xivocc_pack_reporting_1     /bin/sh -c echo "WEEKS_TO_ ...   Up                                                                       
  xivocc_pgxivocc_1           /docker-entrypoint.sh postgres   Up       0.0.0.0:5443->5432/tcp                                          
  xivocc_postgresvols_1       /bin/bash                        Exit 0                                                                   
  xivocc_recording_rsync_1    /usr/local/sbin/run-rsync.sh     Up       0.0.0.0:873->873/tcp                                            
  xivocc_recording_server_1   bin/recording-server-docker      Up       0.0.0.0:9400->9000/tcp                                          
  xivocc_spagobi_1            /bin/sh -c /root/start.sh        Up       0.0.0.0:9500->8080/tcp                                          
  xivocc_timezone_1           /bin/bash                        Exit 0                                                                   
  xivocc_xivo_replic_1        /usr/local/bin/start.sh /o ...   Up                                                                       
  xivocc_xivo_stats_1         /usr/local/bin/start.sh /o ...   Up                                                                       
  xivocc_xivocclogs_1         /bin/bash                        Exit 0                                                                   
  xivocc_xuc_1                bin/xuc_docker                   Up       0.0.0.0:8090->9000/tcp                                          
  xivocc_xucmgt_1             bin/xucmgt_docker                Up       0.0.0.0:8070->9000/tcp                                          


Find the installed version
++++++++++++++++++++++++++

Compoments version can be found in the log files and at the bottom right corner on the web pages.
You may also get the version from the docker container itself by typing:

.. code-block:: bash

    docker exec -ti xivocc_xucmgt_1 cat /opt/docker/conf/appli.version

Change ``xivocc_xucmgt_1`` by the name of the component you want to check.

Fingerboard
-----------

The home page of XiVO-CC is available using the following url: ``http://<XiCO-CC-IP>:80``

.. note:: Replace <XiVO-CC-IP> by the **real** XiVO-CC IP address of your server.

.. figure:: images/fingerboard.png
   :scale: 100%

To be check
+++++++++++

* The web page loads properly.

Configuration Management
------------------------

Login
+++++

The configuration manager is avalable at ``http://<XiCO-CC-IP>:9100``

.. note:: Replace <XiVO-CC-IP> by the **real** XiVO-CC IP address of your server.

Bellow the default login informations :

+------------+------------+
| Login      | avencall   |
+------------+------------+
| Password   | superpass  |
+------------+------------+

Post Installation
+++++++++++++++++

Create the user ``xuc`` with the administrator profile in the interface, it will set the right to the
``xuc`` user that you previously created on the XiVO PBX.

Create an other user as an administrator profile in the interface.

To be check
+++++++++++

* When using the search bar, the XiVO PBX users are correctly displayed.

SpagoBI
-------

Login
+++++

SpagoBi is avalable at ``http://<XiCO-CC-IP>:9500/SpagoBI``

.. note:: Replace <XiVO-CC-IP> by the **real** XiVO-CC IP address of your server.

.. note:: The url is case sensitive.

Bellow the default login informations :

+------------+------------+
| Login      | biadmin    |
+------------+------------+
| Password   | biadmin    |
+------------+------------+

Post installation
+++++++++++++++++

Change the default language
^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default the SpagoBI interface is in English, you can change it by following the instructions
below:

Go to :menuselection:`⚙ Resources --> Configuration management`.

In the :menuselection:`Select Category` field, chose :menuselection:`LANGUAGE_SUPPORTED` and change
value of the label :menuselection:`SPAGOBI.LANGUAGE_SUPPORTED.LANGUAGE.default` to your language :
fr,FR, en,US, ...

Install sample reports
^^^^^^^^^^^^^^^^^^^^^^

* Download the sample reports from 
  https://gitlab.com/xivocc/sample_reports/raw/master/spagobi/standardreports.zip
* Go to :menuselection:`☴ Tools --> Import/export`
* In the import section select the zip file
* Use all the default options exept for the engine, you must choose
  :menuselection:`Jaster Report Engine` as engine

.. note:: The zip file should be exactly named standardreports.zip

To be check
+++++++++++

Use the database status report to check if replication and reporting generation is working:

.. figure:: images/spagobi_datastatus.png
   :scale: 75%

You can view the data in a sample report:

.. figure:: images/spagobi_reportsample.png
    :scale: 50%

.. note:: The data replication can take some time if there are a lot of entries in XiVO PBX CEL and
   queue_log tables.

Totem Panels
------------

Import the default panels
+++++++++++++++++++++++++

There are preconfigured panels shipped with default installation, they are avalaible by using the 
following urls :

* agent panel: ``http://<XiVO-CC-IP>/kibana/#/dashboard/file/agents.json``
* queue panel: ``http://<XiVO-CC-IP>/kibana/#/dashboard/file/queues.json``

Bellow the default login informations :

+------------+------------+
| Login      | admin      |
+------------+------------+
| Password   | Kibana     |
+------------+------------+

To access faster those panels, you can save them with the following steps:

* on the queue or agent panel, click on the floppy disk
* enter the name of the panel and click save button
* enter the login informations

The panel is now saved and accessible by using folder icon.


Bellow an example of panel:

.. figure:: images/totempanel.png
    :scale: 50%

To be check
+++++++++++

Call statistics in the panels should vary in real time.

.. note:: The data replication can take some time if there are a lot of entries in XiVO PBX CEL and
   queue_log tables.

*************************
Validate the Installation
*************************

You will find below the list of steps to validate to make sure that your installation is fully functional:

* The check in the Post-installation were fuctional
* XuC internal database is synchronized with XiVO PBX status page:
   * Go to: ``http://<XiVO-CC-IP>:8090``
   * Verify tha you have all your agents an queues in the *Internal configuration cache database*
* CCManager is functioning
   * Go to: ``http://<XiVO-CC-IP>:8070/ccmanager``
   * Login with a xivo user having a cti profile
   * Check that the queues and agents configured on the system properly appears.
   * Try to log and delog an agent.
* Agent Web interface is functioning:
   * Go to: ``http://<XiVO-CC-IP>:8070/agent``
   * Login a agent.
   * Change his status.
* Web assistant functioning:
   * Go to: ``http://<XiVO-CC-IP>:8070/agent``
   * Login a user.
   * Check that call history is not empty.
* Check elasticsearch database status:

Run ``curl -XGET 127.0.0.1:9200/queuelogs/_status | python -m json.tool``, you will have the following output:

.. code-block:: json

  {
      "_shards": {
          "failed": 0,
          "successful": 5,
          "total": 10
      },
      "indices": {
          "queuelogs": {
              "docs": {
                  "deleted_docs": 0,
                  "max_doc": 546702,
                  "num_docs": 546702
              },
  ...

You should not have failed ``_shards`` and ``num_doc`` shoud be > 0

* Check that the recording processes are working:
   * Go to: ``http://<XiVO-CC-IP>:9400``
   * Login a user.
   * Check that the calls recorded appears properly.
   * Check you are able to download a call record.

