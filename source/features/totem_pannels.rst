Totem Panel
===========

Totem panels are using Kibana witch a web tool used to compute statistics based on Elasticsearch
content.

Bellow a sample panel:

.. figure:: totempanel.png
   :scale: 50%
   :alt: Kibana sample panel

Graphs are based on the queue_log table, enriched with agent names and agent groups, and inserted
into an Elasticsearch index.

It contains avents about calls placed on queues, and events about agent presences.

For each entry in the queue_log index, the following attributes are available:

* queudisplayname : Queue display name
* data1: basic queue_log data, with a different meaning according to the event
* callid : Call unique identifier, generated by Asterisk
* event : Call or agent status event - please see below
* agentnumber: Agent number
* queuename : Technical queue name
* groupname : Agent group name
* queuetime: Time of the event
* agentname : Name of the agent, if available

The event can be one of the following (for a detailed explanation, please refer to https://wiki.asterisk.org/wiki/display/AST/Queue+Logs):

* Call events:

  * FULL
  * CONNECT
  * EXITEMPTY
  * CLOSED
  * EXITWITHTIMEOUT
  * JOINEMPTY
  * ABANDON
  * ENTERQUEUE
  * TRANSFER
  * COMPLETEAGENT
  * COMPLETECALLER
  * RINGNOANSWER

* Agent or queue event:

  * ADDMEMBER
  * PAUSEALL
  * PAUSE
  * WRAPUPSTART
  * UNPAUSE
  * UNPAUSEALL
  * PENALTY
  * CONFIGRELOAD
  * AGENTCALLBACKLOGIN
  * AGENTCALLBACKLOGOFF
  * REMOVEMEMBER
  * PRESENCE
  * QUEUESTART
